mod.wizards.newContentElement.wizardItems.basecontent {
	header = LLL:EXT:basecontent/Resources/Private/Language/locallang_db_new_content_el.xlf:basecontent
	elements {
		slider {
			icon = ../typo3conf/ext/basecontent/Resources/Public/Icons/slideshow-wiz.png
			title = LLL:EXT:basecontent/Resources/Private/Language/locallang_db_new_content_el.xlf:basecontent_slider_title
			description = LLL:EXT:basecontent/Resources/Private/Language/locallang_db_new_content_el.xlf:basecontent_slider_description
			tt_content_defValues {
				CType = basecontent_slider
			}
		}
	}
	show = *
}
