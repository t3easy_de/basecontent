<?php
if (!defined('TYPO3_MODE')) {
	die ('Access denied.');
}
$extRelPath = \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extRelPath($_EXTKEY);

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
	'T3easy.' . $_EXTKEY,
	'Slider',
	'basecontent Slider',
	$extRelPath . 'Resources/Public/Icons/slideshow.png'
);

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile($_EXTKEY, 'Configuration/TypoScript', 'basecontent');

$TCA['tt_content']['types'][$_EXTKEY . '_slider']['showitem'] = '
	--palette--;LLL:EXT:cms/locallang_ttc.xml:palette.general;general,
	--palette--;LLL:EXT:cms/locallang_ttc.xml:palette.header;header,
	LLL:EXT:cms/locallang_ttc.xml:tabs.images, image,
	--div--;LLL:EXT:cms/locallang_ttc.xml:tabs.access,
		--palette--;LLL:EXT:cms/locallang_ttc.xml:palette.visibility;visibility,
		--palette--;LLL:EXT:cms/locallang_ttc.xml:palette.access;access
	';

$TCA['tt_content']['ctrl']['typeicons'][$_EXTKEY . '_slider'] = $extRelPath . 'Resources/Public/Icons/slideshow.png';
$TCA['tt_content']['ctrl']['typeicon_classes'][$_EXTKEY . '_slider'] = 'extensions-' . $_EXTKEY . '-type-slider';

if (TYPO3_MODE == 'BE') {
	$icons = array(
		'type-slider' => $extRelPath . 'Resources/Public/Icons/slideshow.png'
	);
	\TYPO3\CMS\Backend\Sprite\SpriteManager::addSingleIcons($icons, $_EXTKEY);
}

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig(
	'<INCLUDE_TYPOSCRIPT: source="FILE:EXT:' . $_EXTKEY . '/Configuration/TSconfig/Page.txt">'
);

?>